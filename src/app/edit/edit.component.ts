import { Component, OnInit } from '@angular/core';
import { StudentService } from '../service/student-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  currentUser: any;
  name: string;
  surname: string;
  image: string;
  birthday: any;
  studentId: string;
  email: string;
  password: string;
  activities: string[];

  constructor(private studentService: StudentService, private router: Router) {

  }

  ngOnInit() {

    this.currentUser = JSON.parse(localStorage.getItem('student'));
    this.name = this.currentUser.name;
    this.surname = this.currentUser.surname;
    this.image = this.currentUser.image;
    this.birthday = this.currentUser.birthday;
    this.studentId = this.currentUser.studentId;
    this.email = this.currentUser.email;
    this.password = this.currentUser.password;
    this.activities = this.currentUser.activities;
  }

  update() {
    let record = {};

    record['name'] = this.name ? this.name : this.currentUser.name;
    record['surname'] = this.surname ? this.surname : this.currentUser.surname;
    record['image'] = this.image ? this.image : this.currentUser.image;
    record['birthday'] = this.birthday ? this.birthday : this.currentUser.birthday;
    record['studentId'] = this.studentId ? this.studentId : this.currentUser.studentId;
    record['email'] = this.email ? this.email : this.currentUser.email;
    record['password'] = this.password ? this.password : this.currentUser.password;
    record['activities'] = this.activities ? this.activities : this.currentUser.activities;

    this.studentService.update(record, this.currentUser.id).then(resp => {
      this.name = "";
      this.surname = "";
      this.image = "";
      this.birthday = "";
      this.studentId = "";
      this.email = "";
      this.password = "";
      this.activities = [];
      this.router.navigateByUrl('/login');
      localStorage.removeItem('student');
    })
      .catch(error => {
        console.log(error);
      });
  }
}
